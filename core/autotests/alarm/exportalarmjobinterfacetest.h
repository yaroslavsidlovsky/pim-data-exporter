/*
   SPDX-FileCopyrightText: 2020-2021 Laurent Montel <montel@kde.org>

   SPDX-License-Identifier: GPL-2.0-or-later
*/

#ifndef ExportAlarmJobInterfaceTest_H
#define ExportAlarmJobInterfaceTest_H

#include <QObject>

class ExportAlarmJobInterfaceTest : public QObject
{
    Q_OBJECT
public:
    explicit ExportAlarmJobInterfaceTest(QObject *parent = nullptr);
    ~ExportAlarmJobInterfaceTest() override = default;
private Q_SLOTS:
    void exportAlarm();
    void exportAlarm_data();
};

#endif // ExportAlarmJobInterfaceTest_H
